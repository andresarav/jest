# Curso de Pruebas unitarias con Jest

Necesitamos instalar las dependencias necesarias para hacer test a los componentes, estas son:

>`npm i -D jest enzyme enzyme-adapter-react-16`

- enzyme: Es una librería creada por airbnb para facilitar el test a componentes en React
- enzyme-adapter-react-16: Es un adaptador para la versión de React que estemos utilizando.


## Configuración en package.json

Le pasamos la configuración del adaptador al projecto.

>`"jest": {
  "setupFilesAfterEnv": [
    "<rootDir>/src/__test__/setupTest.js"
  ]
}`
